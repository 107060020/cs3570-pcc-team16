import logging.config
import numpy as np
import yaml

from utils.file_io import get_logging_config
from algs_wrapper.Draco import Draco
from algs_wrapper.PCL import PCL

def setRate_Draco(cl, qp):
    with open('cfgs/algs/Draco.yml') as f:
        rate = yaml.load(f, Loader=yaml.FullLoader)
    rate['r1']['cl'] = cl
    rate['r1']['qp'] = qp
    with open('cfgs/algs/Draco.yml', 'w') as f:
        yaml.dump(rate, f)

def setRate_PCL(PR, OR):
    with open('cfgs/algs/PCL.yml') as f:
        rate = yaml.load(f, Loader=yaml.FullLoader)
    rate['r1']['PR'] = PR
    rate['r1']['OR'] = OR
    with open('cfgs/algs/PCL.yml', 'w') as f:
        yaml.dump(rate, f)

def main():
    LOGGING_CONFIG = get_logging_config('utils/logging.conf')
    logging.config.dictConfig(LOGGING_CONFIG)

    
    with open('record_Draco.csv', 'w') as f:
        f.write('cl,qp,Encoding time (s),Decoding time (s),Source point cloud size (kB),Total binary files size (kB),Compression ratio,bpp,Y-CPSNR (dB),U-CPSNR (dB),V-CPSNR (dB),Hybrid geo-color,score\n')
    with open('record_PCL.csv', 'w') as f:
        f.write('PR,OR,Encoding time (s),Decoding time (s),Source point cloud size (kB),Total binary files size (kB),Compression ratio,bpp,Y-CPSNR (dB),U-CPSNR (dB),V-CPSNR (dB),Hybrid geo-color,score\n')
        
    with open('record_Draco.csv', 'a') as f:
        f.write(str(10)+','+str(29)+',')
    setRate_Draco(10, 29)
    draco = Draco()
    draco.debug = True
    draco.rate = 'r1'
    draco.run_dataset('Debug_SNCC', 'experiments')


    with open('record_PCL.csv', 'a') as f:
        f.write(str(0.0001)+','+str(0.01)+',')
    setRate_PCL(0.0001, 0.01)
    pcl = PCL()
    pcl.debug = True
    pcl.rate = 'r1'
    pcl.run_dataset('Debug_SNCC', 'experiments')
    
    

if __name__ == '__main__':
    main()
