import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

csv = pd.read_csv('record_Draco.csv')
X = csv['cl'].values.reshape(-1, 31)
Y = csv['qp'].values.reshape(-1, 31)
Z = np.array(csv['bpp'].values).reshape(-1, 31)

fig = plt.figure(figsize=(8, 6))
ax = Axes3D(fig)
surf = ax.plot_surface(np.log10(X), np.log10(Y), np.log10(Z),
                       rstride=1, cstride=1, cmap='rainbow')
fig.colorbar(surf, ax=ax, pad=0.1, shrink=0.8)
ax.set_title('Draco Compression', fontsize=13)
ax.set_xlabel('Compression Level', fontsize=13)
ax.set_ylabel('Quantization Bits for Position', fontsize=13)
ax.set_zlabel('Bit Per Point', fontsize=13)
plt.show()

csv = pd.read_csv('record_Draco.csv')
X = csv['cl'].values.reshape(-1, 31)
Y = csv['qp'].values.reshape(-1, 31)
Z = np.array(csv['Hybrid geo-color'].values).reshape(-1, 31)

fig = plt.figure(figsize=(8, 6))
ax = Axes3D(fig)
surf = ax.plot_surface(np.log10(X), np.log10(Y), np.log10(Z),
                       rstride=1, cstride=1, cmap='rainbow')
fig.colorbar(surf, ax=ax, pad=0.1, shrink=0.8)
ax.set_title('Draco Compression', fontsize=13)
ax.set_xlabel('Compression Level', fontsize=13)
ax.set_ylabel('Quantization Bits for Position', fontsize=13)
ax.set_zlabel('Hybrid geo-color', fontsize=13)
plt.show()

csv = pd.read_csv('record_Draco.csv')
X = csv['cl'].values.reshape(-1, 31)
Y = csv['qp'].values.reshape(-1, 31)
Z = np.array(csv['Encoding time (s)'].values).reshape(-1, 31)

fig = plt.figure(figsize=(8, 6))
ax = Axes3D(fig)
surf = ax.plot_surface(np.log10(X), np.log10(Y), np.log10(Z),
                       rstride=1, cstride=1, cmap='rainbow')
fig.colorbar(surf, ax=ax, pad=0.1, shrink=0.8)
ax.set_title('Draco Compression', fontsize=13)
ax.set_xlabel('Compression Level', fontsize=13)
ax.set_ylabel('Quantization Bits for Position', fontsize=13)
ax.set_zlabel('Encoding Time (s)', fontsize=13)
plt.show()


csv = pd.read_csv('record_Draco.csv')
X = csv['cl'].values.reshape(-1, 31)
Y = csv['qp'].values.reshape(-1, 31)
Z = np.array(csv['Decoding time (s)'].values).reshape(-1, 31)

fig = plt.figure(figsize=(8, 6))
ax = Axes3D(fig)
surf = ax.plot_surface(np.log10(X), np.log10(Y), np.log10(Z),
                       rstride=1, cstride=1, cmap='rainbow')
fig.colorbar(surf, ax=ax, pad=0.1, shrink=0.8)
ax.set_title('Draco Compression', fontsize=13)
ax.set_xlabel('Compression Level', fontsize=13)
ax.set_ylabel('Quantization Bits for Position', fontsize=13)
ax.set_zlabel('Decoding Time (s)', fontsize=13)
plt.show()

csv = pd.read_csv('record_Draco.csv')
X = csv['cl'].values.reshape(-1, 31)
Y = csv['qp'].values.reshape(-1, 31)
Z = np.array(csv['score'].values).reshape(-1, 31)
fig = plt.figure(figsize=(8, 6))
ax = Axes3D(fig)
surf = ax.plot_surface(np.log10(X), np.log10(Y), np.log10(Z),
                       rstride=1, cstride=1, cmap='rainbow')
fig.colorbar(surf, ax=ax, pad=0.1, shrink=0.8)
ax.set_title('Draco Compression', fontsize=13)
ax.set_xlabel('Compression Level', fontsize=13)
ax.set_ylabel('Quantization Bits for Position', fontsize=13)
ax.set_zlabel(r'$\frac{1}{bpp\ *\ Hybrid\ geo-color}$', fontsize=16)
plt.show()

csv = pd.read_csv('record_PCL.csv')
X = csv['PR'].values.reshape(-1, 18)
Y = csv['OR'].values.reshape(-1, 18)
Z = np.array(csv['bpp'].values).reshape(-1, 18)

fig = plt.figure(figsize=(8, 6))
ax = Axes3D(fig)
surf = ax.plot_surface(np.log10(X), np.log10(Y), np.log10(Z),
                       rstride=1, cstride=1, cmap='rainbow')
fig.colorbar(surf, ax=ax, pad=0.1, shrink=0.8)
ax.set_title('PCL Compression', fontsize=13)
ax.set_xlabel('Point Resolution', fontsize=13)
ax.set_ylabel('Octree Resolution', fontsize=13)
ax.set_zlabel('Bit Per Point', fontsize=13)
plt.show()

csv = pd.read_csv('record_PCL.csv')
X = csv['PR'].values.reshape(-1, 18)
Y = csv['OR'].values.reshape(-1, 18)
Z = np.array(csv['Hybrid geo-color'].values).reshape(-1, 18)

fig = plt.figure(figsize=(8, 6))
ax = Axes3D(fig)
surf = ax.plot_surface(np.log10(X), np.log10(Y), np.log10(Z),
                       rstride=1, cstride=1, cmap='rainbow')
fig.colorbar(surf, ax=ax, pad=0.1, shrink=0.8)
ax.set_title('PCL Compression', fontsize=13)
ax.set_xlabel('Point Resolution', fontsize=13)
ax.set_ylabel('Octree Resolution', fontsize=13)
ax.set_zlabel('Hybrid geo-color', fontsize=13)
plt.show()

csv = pd.read_csv('record_PCL.csv')
X = csv['PR'].values.reshape(-1, 18)
Y = csv['OR'].values.reshape(-1, 18)
Z = np.array(csv['Encoding time (s)'].values).reshape(-1, 18)

fig = plt.figure(figsize=(8, 6))
ax = Axes3D(fig)
surf = ax.plot_surface(np.log10(X), np.log10(Y), np.log10(Z),
                       rstride=1, cstride=1, cmap='rainbow')
fig.colorbar(surf, ax=ax, pad=0.1, shrink=0.8)
ax.set_title('PCL Compression', fontsize=13)
ax.set_xlabel('Point Resolution', fontsize=13)
ax.set_ylabel('Octree Resolution', fontsize=13)
ax.set_zlabel('Encoding Time (s)', fontsize=13)
plt.show()

csv = pd.read_csv('record_PCL.csv')
X = csv['PR'].values.reshape(-1, 18)
Y = csv['OR'].values.reshape(-1, 18)
Z = np.array(csv['Decoding time (s)'].values).reshape(-1, 18)

fig = plt.figure(figsize=(8, 6))
ax = Axes3D(fig)
surf = ax.plot_surface(np.log10(X), np.log10(Y), np.log10(Z),
                       rstride=1, cstride=1, cmap='rainbow')
fig.colorbar(surf, ax=ax, pad=0.1, shrink=0.8)
ax.set_title('PCL Compression', fontsize=13)
ax.set_xlabel('Point Resolution', fontsize=13)
ax.set_ylabel('Octree Resolution', fontsize=13)
ax.set_zlabel('Decoding Time (s)', fontsize=13)
plt.show()

csv = pd.read_csv('record_PCL.csv')
X = csv['PR'].values.reshape(-1, 18)
Y = csv['OR'].values.reshape(-1, 18)
Z = np.array(csv['score'].values).reshape(-1, 18)

fig = plt.figure(figsize=(8, 6))
ax = Axes3D(fig)
surf = ax.plot_surface(np.log10(X), np.log10(Y), np.log10(Z),
                       rstride=1, cstride=1, cmap='rainbow')
fig.colorbar(surf, ax=ax, pad=0.1, shrink=0.8)
ax.set_title('PCL Octree Compression', fontsize=13)
ax.set_xlabel('Point Resolution', fontsize=13)
ax.set_ylabel('Octree Resolution', fontsize=13)
ax.set_zlabel(r'$\frac{1}{bpp\ *\ Hybrid\ geo-color}$', fontsize=16)
plt.show()
