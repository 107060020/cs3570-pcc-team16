from algs_wrapper.base import Base

class PCL(Base):
    def __init__(self):
        super().__init__()

    def make_encode_cmd(self, in_pcfile, bin_file):
        cmd = [
            self._algs_cfg['encoder'],
            '-i', in_pcfile,
            '-o', bin_file,
            '-PR', str(self._algs_cfg[self.rate]['PR']),
            '-OR', str(self._algs_cfg[self.rate]['OR']),
            '-DS', str(self._algs_cfg[self.rate]['DS']),
            '-FR', str(self._algs_cfg[self.rate]['FR']),
            '-BR', str(self._algs_cfg[self.rate]['BR']),
            '-CE', str(self._algs_cfg[self.rate]['CE'])
        ]
        
        return cmd

    def make_decode_cmd(self, bin_file, out_pcfile):
        cmd = [
            self._algs_cfg['decoder'],
            '-i', bin_file,
            '-o', out_pcfile
        ]

        return cmd
