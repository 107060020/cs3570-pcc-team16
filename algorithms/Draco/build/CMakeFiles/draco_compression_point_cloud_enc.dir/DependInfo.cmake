# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/yucheng/Desktop/cs3570-pcc-team16/algorithms/Draco/src/draco/compression/point_cloud/point_cloud_encoder.cc" "/home/yucheng/Desktop/cs3570-pcc-team16/algorithms/Draco/build/CMakeFiles/draco_compression_point_cloud_enc.dir/src/draco/compression/point_cloud/point_cloud_encoder.cc.o"
  "/home/yucheng/Desktop/cs3570-pcc-team16/algorithms/Draco/src/draco/compression/point_cloud/point_cloud_kd_tree_encoder.cc" "/home/yucheng/Desktop/cs3570-pcc-team16/algorithms/Draco/build/CMakeFiles/draco_compression_point_cloud_enc.dir/src/draco/compression/point_cloud/point_cloud_kd_tree_encoder.cc.o"
  "/home/yucheng/Desktop/cs3570-pcc-team16/algorithms/Draco/src/draco/compression/point_cloud/point_cloud_sequential_encoder.cc" "/home/yucheng/Desktop/cs3570-pcc-team16/algorithms/Draco/build/CMakeFiles/draco_compression_point_cloud_enc.dir/src/draco/compression/point_cloud/point_cloud_sequential_encoder.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../src"
  "."
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
