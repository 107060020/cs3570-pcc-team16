file(REMOVE_RECURSE
  "libdracoenc.a"
  "libdracoenc.pdb"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/dracoenc.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
