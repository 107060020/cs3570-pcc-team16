#include <pcl/compression/octree_pointcloud_compression.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/point_cloud.h>
#include <pcl/io/ply_io.h>
#include <iostream>

struct Options {
    Options(std::string i, std::string o, bool SS,double PR, double OR, bool DS, int FR, bool CE, int BR):
        input(i), output(o), showStatistics(SS), pointResolution(PR), octreeResolution(OR), doVoxelGridDownSampling(DS), iFrameRate(FR), doColorEncoding(CE), colorBitResolution(BR) {}
    std::string input;
    std::string output;
    bool showStatistics;
    double pointResolution;
    double octreeResolution;
    bool doVoxelGridDownSampling;
    int iFrameRate;
    bool doColorEncoding;
    int colorBitResolution;
};

void Usage() {
  printf("Usage:\n");
  printf("  -h | -?               show help.\n");
  printf("  -i <input>            input file name.\n");
  printf("  -o <output>           output file name.\n");
  printf("Optional (encoder):\n");
  printf("  -SS <true/false>      show statistics.\n");
  printf("  -PR <float>           point resolution.\n");
  printf("  -OR <float>           octree resolution.\n");
  printf("  -DS <true/false>      do voxel grid down sampling.\n");
  printf("  -FR <unsigned int>    I frame rate.\n");
  printf("  -CE <true/false>      do color encoding.\n");
  printf("  -BR <unsigned int>    color bit resolution.\n");
  
}

int main(int argc, char **argv)
{
    Options options{"default.ply", "default.bin", true, 0.0001, 0.01, false, 100, true, 8};
    options.showStatistics = true;

    const int argc_check = argc - 1;
    for (int i = 1; i < argc; ++i) {
        if (!strcmp("-h", argv[i]) || !strcmp("-?", argv[i])) {
            Usage();
            return 0;
        } else if (!strcmp("-i", argv[i]) && i < argc_check) {
            options.input = argv[++i];
        } else if (!strcmp("-o", argv[i]) && i < argc_check) {
            options.output = argv[++i];
        } else if (!strcmp("-SS", argv[i]) && i < argc_check) {
            options.showStatistics = (atoi(argv[++i]) == 1) ? true : false;
        } else if (!strcmp("-PR", argv[i]) && i < argc_check) {
            options.pointResolution = atof(argv[++i]);
        } else if (!strcmp("-OR", argv[i]) && i < argc_check) {
            options.octreeResolution = atof(argv[++i]);
        } else if (!strcmp("-DS", argv[i]) && i < argc_check) {
            options.doVoxelGridDownSampling = (atoi(argv[++i]) == 1) ? true : false;
        } else if (!strcmp("-FR", argv[i]) && i < argc_check) {
            options.iFrameRate = atof(argv[++i]);
        } else if (!strcmp("-CE", argv[i]) && i < argc_check) {
            options.doColorEncoding = (atoi(argv[++i]) == 1) ? true : false;
        } else if (!strcmp("-BR", argv[i]) && i < argc_check) {
            options.colorBitResolution = atoi(argv[++i]);
        } 
    }   

    if (argc < 5 || options.input.empty()) {
        Usage();
        return -1;
    }

    pcl::io::OctreePointCloudCompression<pcl::PointXYZRGB>* PointCloudDecoder;
    PointCloudDecoder = new pcl::io::OctreePointCloudCompression<pcl::PointXYZRGB>(
        pcl::io::MANUAL_CONFIGURATION, 
        options.showStatistics, 
        options.pointResolution,
        options.octreeResolution,
        options.doVoxelGridDownSampling,
        options.iFrameRate,
        options.doColorEncoding,
        options.colorBitResolution
        );

    ifstream bitstreamFile(options.input, ios::binary);
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr outPly(new pcl::PointCloud<pcl::PointXYZRGB>());
    PointCloudDecoder->decodePointCloud(bitstreamFile, outPly);
    bitstreamFile.close();

    pcl::PLYWriter writer;
    writer.write(options.output, *outPly, true, false);

    return 0;
}