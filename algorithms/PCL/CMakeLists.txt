
cmake_minimum_required( VERSION 2.8 )

# Create Project
project( solution )


# Set StartUp Project (Option)
# (This setting is able to enable by using CMake 3.6.0 RC1 or later.)
set_property( DIRECTORY PROPERTY VS_STARTUP_PROJECT "project" )

# Find Packages
find_package( PCL REQUIRED )


include_directories( ${PCL_INCLUDE_DIRS} )
add_definitions( ${PCL_DEFINITIONS} )
link_directories( ${PCL_LIBRARY_DIRS} )

add_executable( encoder ../encoder.cc )
target_link_libraries( encoder ${PCL_LIBRARIES} )
add_executable( decoder ../decoder.cc )
target_link_libraries( decoder ${PCL_LIBRARIES} )

