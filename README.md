# An overview of two signal-processing based point cloud compression method 
[Course Page](https://nmsl.cs.nthu.edu.tw/cs3570-schedule-2021/)

[TA session slides](https://nmsl.cs.nthu.edu.tw/wp-content/uploads/2021/03/Challenge2.pdf)

## Team members

| Student ID |  Name  |
| :--------: | :----: |
| 107060023  | 黃子恩 |
| 107060020  | 洪瑜成 |
| 107060007  | 王順貴 |
## Environments
Ubuntu 20.04

## Prerequisites
* PCL   
* Ananconda 3
* git
* gcc
* g++

```bash=
wget https://repo.anaconda.com/archive/Anaconda3-2020.11-Linux-x86_64.sh
sh Anaconda3-2020.11-Linux-x86_64.sh
source ~/.bashrc
sudo apt install git gcc g++ -y
```
Detailed instructions please refered to the slides.

## PCL installation
Since python>=3.8 does not support many dependencies of PCL, PCL and its dependencies need to be installed locally with python<=3.7, not in anaconda. Our anaconda environment 'pcc_arena' uses python3.9.

An useful website: https://blog.csdn.net/u014301278/article/details/102947451

## Installation
```bash=
git clone https://gitlab.com/107060020/cs3570-pcc-team16.git
cd cs3570-pcc-team16
conda env create -f cfgs/conda_env/pcc_arena.yml
conda activate pcc_arena
python setup.py
```
Download `mpeg-pcc-dmetric-master.tar.gz` from [eLearn](https://elearn.nthu.edu.tw/mod/resource/view.php?id=31207).

Download datasets and pretrained models from [Google Drive](https://drive.google.com/drive/folders/1rf088BC1oCXlLijOy39eT4DsnD_OcMsw?usp=sharing)

Put all the files you downloaded under `cs3570-pcc-team16/`

```bash=
./setup_env_ds.sh
```

## Abstract
Recently, point cloud become more popular for scientific, engineering and other applications. However, raw point cloud file usually not suitable for distribution from end to end, thus, a good compression method is essential. In our work, we explore the core idea of Draco and Point Cloud Library (PCL) and how the parameter we used affect the result on the specific datasets. 

## Introduction
We use two signal-processing based methods, Google Draco and Point Cloud Library (PCL), to take point cloud compression on a dataset provided by TA. The dataset consists of several (precisely, 100 in total) small 3D point cloud models with different size. Besides using the default parameter suggested by Draco or PCL, we also conducted experiments on a wide range of parameters. Though it may take a tremendous of time to test a wide range of parameters, we believed that we could find something interesting after visualizing the results. 

In summary, the contributions of our project are: 
    Find a reasonable parameter that balance the compression quality and the encoding/decoding time on 3D point cloud models. 

* Visualize the results of a wide range of parameter of Draco and PCL that people may not have the opportunity to access to. 
* Prevent people from wasting time if the dataset of others is similar to ours. 
* Analyze pros and cons. using different parameters. 
* Show some interesting phenomena when using the specific range of parameters. 

## Related work
### Google Draco 
The Google Draco is a state-of-art codec for compressing point cloud and mesh file with/without color, it uses the rANS entropy encode algorithm to compress the geometric and color information. 

[Draco repo.](https://github.com/google/draco)

### Point Cloud Library 
Point Cloud Library (PCL) is a library for operating point clouds, it includes I/O, octree, KD-tree etc. for manipulate the point cloud data. 

[PCL repo.](https://github.com/PointCloudLibrary/pcl)

## Method
### Metrics 
* Bit Per Point 
* Hybrid geo-color 
* Score 
![](https://i.imgur.com/NYYwqQo.png)


### Draco 
* **Compression Level**
Compression level (flag -cl) is one of the primary parameters that affect the point cloud compression. The value of it ranging from 0 to 10, and each of them represents different compression strategy to compress mesh or point cloud. 

    In general, 
    * Minimum setting 0 will have the least compression, but best encoding/decoding speed. 

    * Maximum setting 0 will have the best compression, but worst encoding/decoding speed. 

    * The higher the compression level (flag -cl) setting, the more complicated the compression features (or algorithms) 

* **Quantization Bits for Position** 
Quantization bits for position (–qp, a built-in flag) is the second primary parameters that affect the point cloud compression. The value of it ranging from 0 to 30, and each of them represents quantizing the input values for the specified attribute to that number of bits. 

    In general, 
    * Minimum setting 0 will not perform any quantization on the specified attribute. 

    * Any value except of 0 will quantize the input values for the specified attribute to that number of bits. 

    * The higher the quantization bits for position setting, the better compression rate you will get. 

In our project, we conducted experiments on parameter –cl ranging from 0 to 10 and parameter –qp ranging from 0 to 30. That is, 341 different combinations in total. 

### PCL 
* **Point Resolution** 
Point resolution (–PR, a custom flag, PCL doesn’t provide a built-in flag) is one of the primary parameters that affect the point cloud compression. There’s no rigorous limitation of its value. It defines coding precision for point coordinates, and it shouldn’t be set to a value above the sensor noise. 
    
    In general, a smaller value will lead to better compression, but worse encoding/decoding speed.  

* **Octree Resolution** 
Octree resolution (–OR, a custom flag, PCL doesn’t provide a built-in flag) is the second primary parameters that affect the point cloud compression. There’s no rigorous limitation of its value. It defines the length of one side of a voxel. 

    In general, a smaller value will lead to better compression, but longer encoding/decoding speed. 

In our project, we use ![](https://i.imgur.com/bfRckt7.png) as parameters for both –PR and –OR. That is, 324 different combinations in total. 


## Run Experiments
Use the following command to run experiments. During execution, two *.csv files will be generated to record the results.
```bash=
python run_experiments.py
```
 It will take more than 2 days to finish it. The two final *.csv files are provided below.
* [Draco results](https://drive.google.com/file/d/1OLCkuTj_huQf53dnY0GI4lUIzGI_kecl/view?usp=sharing)
* [PCL results](https://drive.google.com/file/d/1v5E5Uy2J1nTw6or_xqL7DQqJnaeRz0XW/view?usp=sharing)

Use the following command to visualize the results
```bash=
python plot.py
```
Several 3D surface plots will be generated. See an example below.

![](https://i.imgur.com/8YEQReN.png)
![](https://i.imgur.com/FjiKtwX.png)

Please move on to our final report for details.
* [Final report](https://drive.google.com/file/d/1OLCkuTj_huQf53dnY0GI4lUIzGI_kecl/view?usp=sharing)

Use the following command to run on our best parameters.
```bash=
python run.py
```
